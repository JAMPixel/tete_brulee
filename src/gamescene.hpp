#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <cocos2d.h>

class GameScene : public cocos2d::Scene
{
  // Player
  // Mobs (MobBuilder)
  // Planet(planet generator / tilemap)
  
public:
  // static cocos2d::Scene* createScene();

  virtual bool init() override;
  virtual void update(float dt) override;
  
};


#endif /* GAMESCENE_H */
